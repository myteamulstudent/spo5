﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SPO5
{
    public partial class Form1 : Form
    {
        private Dictionary<int, string> rehash = new Dictionary<int, string>();
        private Dictionary<int, List<string>> chain = new Dictionary<int, List<string>>();

        public Form1()
        {
            InitializeComponent();
        }

        public class TableObject
        {
            public int hash;
            public string value;

            public TableObject(int hash, string value)
            {
                this.hash = hash;
                this.value = value;
            }

            public int Hash
            {
                get { return hash; }
                set { this.hash = value; }
            }

            public string Value
            {
                get { return value; }
                set { this.value = value; }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = rehash.ToArray();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            if (valueTextBox.Text != string.Empty)
            {
                if (rehash.ContainsKey(Hash.HF(valueTextBox.Text)) && rehash[Hash.HF(valueTextBox.Text)] != valueTextBox.Text)
                {
                    int i = 0;
                    while (true)
                    {
                        i++;
                        if (!rehash.ContainsKey(Hash.HF(valueTextBox.Text, i)))
                        {
                            rehash.Add(Hash.HF(valueTextBox.Text, i), valueTextBox.Text);
                            MessageBox.Show(string.Format("Рехеширование - добавил на {0} итерации", i + 1));
                            break;
                        }
                        else if (rehash.ContainsKey(Hash.HF(valueTextBox.Text, i)) && rehash[Hash.HF(valueTextBox.Text, i)] == valueTextBox.Text)
                        {
                            break;
                        }
                    }
                }
                else if (!rehash.ContainsKey(Hash.HF(valueTextBox.Text)))
                    rehash.Add(Hash.HF(valueTextBox.Text), valueTextBox.Text);
                else MessageBox.Show("Не повторяйся");

                if (chain.ContainsKey(Hash.HF(valueTextBox.Text)) && !chain[Hash.HF(valueTextBox.Text)].Contains(valueTextBox.Text))
                {
                    chain[Hash.HF(valueTextBox.Text)].Add(valueTextBox.Text);
                }
                else if (!chain.ContainsKey(Hash.HF(valueTextBox.Text)))
                {
                    chain[Hash.HF(valueTextBox.Text)] = new List<string>();
                    chain[Hash.HF(valueTextBox.Text)].Add(valueTextBox.Text);
                }
            }

            dataGridView1.DataSource = rehash.ToArray();
        }

        private void search_Click(object sender, EventArgs e)
        {
            int hash = Hash.HF(valueTextBox.Text);
            int counterComparesRehash = 0;
            int counterComparesChain = 0;

            rehashElement.Text = "Элемент не найден";
            chainElement.Text = "Элемент не найден";

            Stopwatch swRehash = new Stopwatch();
            swRehash.Start();
            if (rehash.ContainsKey(hash))
            {
                if (rehash[hash] == valueTextBox.Text)
                {
                    counterComparesRehash++;
                    swRehash.Stop();
                }
                else
                {
                    counterComparesRehash++;
                    while (true)
                    {
                        if (!rehash.ContainsKey(Hash.HF(valueTextBox.Text, counterComparesRehash)))
                        {
                            counterComparesRehash++;
                            break;
                        }
                        else if (rehash[Hash.HF(valueTextBox.Text, counterComparesRehash)] == valueTextBox.Text)
                        {
                            rehashElement.Text = "Элемент найден";
                            counterComparesRehash++;
                            break;
                        }
                        counterComparesRehash++;
                    }
                }
            }
            swRehash.Stop();

            Stopwatch swChain = new Stopwatch();
            swChain.Start();
            if (chain.ContainsKey(hash))
            {
                foreach (var value in chain[hash])
                {
                    counterComparesChain++;
                    if (value == valueTextBox.Text)
                    {
                        chainElement.Text = "Элемент найден";
                        break;
                    }
                }
            }

            swChain.Stop();

            timeRehash.Text = swRehash.Elapsed.TotalMilliseconds + " ms";
            timeChain.Text = swChain.Elapsed.TotalMilliseconds + " ms";

            numberCompareChain.Text = counterComparesChain.ToString();
            numberCompareRehash.Text = counterComparesRehash.ToString();

        }

        private void addFromFile_Click(object sender, EventArgs e)
        {
            var words = File.ReadAllText("text.txt").Split(new char[] { ' ', ',', '.', ':', '!', '?', '-' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string word in words)
            {
                if (rehash.ContainsKey(Hash.HF(word)) && rehash[Hash.HF(word)] != word)
                {
                    int i = 0;
                    while (true)
                    {
                        i++;
                        if (!rehash.ContainsKey(Hash.HF(word, i)))
                        {
                            rehash.Add(Hash.HF(word, i), word);
                            //MessageBox.Show(string.Format("Рехеширование - добавил на {0} итерации", i + 1));
                            break;
                        }
                        else if (rehash.ContainsKey(Hash.HF(word, i)) && rehash[Hash.HF(word, i)] == word)
                        {
                            break;
                        }
                    }
                }
                else if (!rehash.ContainsKey(Hash.HF(word)))
                    rehash.Add(Hash.HF(word), word);
                //else MessageBox.Show("Не повторяйся");

                if (chain.ContainsKey(Hash.HF(word)) && !chain[Hash.HF(word)].Contains(word))
                {
                    chain[Hash.HF(word)].Add(word);
                }
                else if (!chain.ContainsKey(Hash.HF(word)))
                {
                    chain[Hash.HF(word)] = new List<string>();
                    if (!chain[Hash.HF(word)].Contains(word))
                        chain[Hash.HF(word)].Add(word);
                }
            }

            dataGridView1.DataSource = rehash.ToArray();
        }


    }
}

