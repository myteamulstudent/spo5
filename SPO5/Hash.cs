﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPO5
{
    class Hash
    {
        public static int HF(string key,int i = 0)
        {
            int hashf = 0;

            if (key.Length == 1)
            {
                hashf = Convert.ToChar(key[0]);
            }
            else
            {
                hashf = Convert.ToChar(key[0] + key[key.Length - 1]);
            }

            return hashf % 201 + i;
        }
    }
}
