﻿namespace SPO5
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.search = new System.Windows.Forms.Button();
            this.valueTextBox = new System.Windows.Forms.TextBox();
            this.Add = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.rehashElement = new System.Windows.Forms.Label();
            this.chainElement = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.numberCompareRehash = new System.Windows.Forms.Label();
            this.numberCompareChain = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timeRehash = new System.Windows.Forms.Label();
            this.timeChain = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.addFromFile = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 375);
            this.dataGridView1.TabIndex = 0;
            // 
            // search
            // 
            this.search.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.search.Location = new System.Drawing.Point(258, 38);
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(142, 60);
            this.search.TabIndex = 1;
            this.search.Text = "Искать";
            this.search.UseVisualStyleBackColor = true;
            this.search.Click += new System.EventHandler(this.search_Click);
            // 
            // valueTextBox
            // 
            this.valueTextBox.Location = new System.Drawing.Point(258, 12);
            this.valueTextBox.Name = "valueTextBox";
            this.valueTextBox.Size = new System.Drawing.Size(289, 20);
            this.valueTextBox.TabIndex = 2;
            // 
            // Add
            // 
            this.Add.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Add.Location = new System.Drawing.Point(418, 38);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(129, 60);
            this.Add.TabIndex = 3;
            this.Add.Text = "Добавить";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(268, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Метод рехеширования";
            // 
            // rehashElement
            // 
            this.rehashElement.AutoSize = true;
            this.rehashElement.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rehashElement.Location = new System.Drawing.Point(279, 180);
            this.rehashElement.Name = "rehashElement";
            this.rehashElement.Size = new System.Drawing.Size(107, 16);
            this.rehashElement.TabIndex = 6;
            this.rehashElement.Text = "Элемент не найден";
            // 
            // chainElement
            // 
            this.chainElement.AutoSize = true;
            this.chainElement.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chainElement.Location = new System.Drawing.Point(439, 180);
            this.chainElement.Name = "chainElement";
            this.chainElement.Size = new System.Drawing.Size(107, 16);
            this.chainElement.TabIndex = 7;
            this.chainElement.Text = "Элемент не найден";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(356, 220);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 16);
            this.label6.TabIndex = 9;
            this.label6.Text = "Кол-во сравнений";
            // 
            // numberCompareRehash
            // 
            this.numberCompareRehash.AutoSize = true;
            this.numberCompareRehash.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numberCompareRehash.Location = new System.Drawing.Point(313, 252);
            this.numberCompareRehash.Name = "numberCompareRehash";
            this.numberCompareRehash.Size = new System.Drawing.Size(14, 16);
            this.numberCompareRehash.TabIndex = 10;
            this.numberCompareRehash.Text = "0";
            // 
            // numberCompareChain
            // 
            this.numberCompareChain.AutoSize = true;
            this.numberCompareChain.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numberCompareChain.Location = new System.Drawing.Point(473, 252);
            this.numberCompareChain.Name = "numberCompareChain";
            this.numberCompareChain.Size = new System.Drawing.Size(14, 16);
            this.numberCompareChain.TabIndex = 11;
            this.numberCompareChain.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(385, 289);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 16);
            this.label2.TabIndex = 12;
            this.label2.Text = "Время";
            // 
            // timeRehash
            // 
            this.timeRehash.AutoSize = true;
            this.timeRehash.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.timeRehash.Location = new System.Drawing.Point(313, 330);
            this.timeRehash.Name = "timeRehash";
            this.timeRehash.Size = new System.Drawing.Size(20, 16);
            this.timeRehash.TabIndex = 13;
            this.timeRehash.Text = "00";
            // 
            // timeChain
            // 
            this.timeChain.AutoSize = true;
            this.timeChain.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.timeChain.Location = new System.Drawing.Point(473, 330);
            this.timeChain.Name = "timeChain";
            this.timeChain.Size = new System.Drawing.Size(20, 16);
            this.timeChain.TabIndex = 14;
            this.timeChain.Text = "00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(439, 143);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 15;
            this.label5.Text = "Метод цепочек";
            // 
            // addFromFile
            // 
            this.addFromFile.Location = new System.Drawing.Point(258, 105);
            this.addFromFile.Name = "addFromFile";
            this.addFromFile.Size = new System.Drawing.Size(288, 23);
            this.addFromFile.TabIndex = 16;
            this.addFromFile.Text = "Добавить из файла";
            this.addFromFile.UseVisualStyleBackColor = true;
            this.addFromFile.Click += new System.EventHandler(this.addFromFile_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 399);
            this.Controls.Add(this.addFromFile);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.timeChain);
            this.Controls.Add(this.timeRehash);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numberCompareChain);
            this.Controls.Add(this.numberCompareRehash);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.chainElement);
            this.Controls.Add(this.rehashElement);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Add);
            this.Controls.Add(this.valueTextBox);
            this.Controls.Add(this.search);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button search;
        private System.Windows.Forms.TextBox valueTextBox;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label rehashElement;
        private System.Windows.Forms.Label chainElement;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label numberCompareRehash;
        private System.Windows.Forms.Label numberCompareChain;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label timeRehash;
        private System.Windows.Forms.Label timeChain;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button addFromFile;
    }
}

